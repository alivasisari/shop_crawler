from django.http import HttpResponse
from django.shortcuts import render
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Products, ProductImages, Store, Discount


class AddProducts(APIView):
    def post(self, request):
        # Get data and save to database table
        print(request.data)
        for product in request.data["Products"]:
            new_product = Products.objects.create(name=product["name"], description=product["description"],
                                                  ok_price=product["okPrice"],
                                                  caption=product["caption"])
            store = Store.objects.create(product=new_product,
                                         name=product["storeName"])
            image = ProductImages.objects.create(product=new_product,
                                                 url=product["thumbImage"])
            discount = Discount.objects.create(product=new_product,
                                               discount=product["discount"])

        return HttpResponse(status=200)


class DisplayProducts(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'products.html'

    def get(self, request):
        # Get data from database tables
        # display data in template
        products = Products.objects.all().values("image__url", "brand__name", "discount__discount", "name",
                                                 "description", "id", "caption", "ok_price")
        context = {'products': products}
        return Response(context)
