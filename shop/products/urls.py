from django.urls import path

from products.views import *

urlpatterns = [
    path('add/', AddProducts.as_view()),
    path('', DisplayProducts.as_view()),
]
