from django.db import models


class Products(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField()
    ok_price = models.IntegerField()
    caption = models.CharField(max_length=250)


class Discount(models.Model):
    product = models.ForeignKey(Products,
                                on_delete=models.SET_NULL,
                                blank=True,
                                null=True, related_name="discount")
    discount = models.IntegerField()


class ProductImages(models.Model):
    product = models.ForeignKey(Products,
                                on_delete=models.SET_NULL,
                                blank=True,
                                null=True, related_name="image")
    url = models.TextField()


class Store(models.Model):
    product = models.ForeignKey(Products,
                                on_delete=models.SET_NULL,
                                blank=True,
                                null=True, related_name="brand")
    name = models.TextField()
