import ast
import asyncio
import aiohttp
from bs4 import BeautifulSoup
import concurrent.futures
from multiprocessing import cpu_count
from threading import current_thread
from ..abstract.crawlerAbstract import CrawlerAbstract


class Crawler(CrawlerAbstract):
    def __init__(self):
        self.url = "https://www.tezolmarket.com"
        self.backend_url = "http://127.0.0.1:8000/products/add/"
        self.urls = set()
        self.home_page_content = None
        self.max_page_number = 1

    async def download(self, page_url, method="POST", params=None) -> str:
        # Send post request to url
        # Get html content
        print(f"Beginning downloading {current_thread()}")
        async with aiohttp.ClientSession() as session:
            if method == "GET":
                async with session.get(url=f"{page_url}") as response:
                    if response.status == 200:
                        content = await response.text()
                    else:
                        raise ValueError('Error')
            elif method == "POST":
                async with session.request(method='POST', url=page_url, params=params) as response:
                    if response.status == 200:
                        content = await response.json()
                    else:
                        raise ValueError('Error')
        print(f"Finished downloading {page_url} {current_thread()}")
        return f"{content}"

    async def send_data(self, params: dict) -> None:
        # Get parsed data
        # Send data to database
        print(f"Begin sending {self.backend_url}")
        async with aiohttp.ClientSession() as session:
            async with session.request(method='POST', url=self.backend_url, json=params) as response:
                if response.status == 200:
                    print("Successful")
                else:
                    raise ValueError('Error to download content')
        print(f"Finished sending {self.backend_url}")

    async def parse_urls(self) -> None:
        # Parse useful data
        parser = BeautifulSoup(self.home_page_content)
        a_tags = parser.find_all("a", href=True)
        for a in a_tags:
            href = a["href"][:-1]
            if "/Category" == href[0:9] and len(href.split("/")) == 3:
                self.urls.add(href)

    async def parse_products(self, products_json: str) -> dict:
        # Parse page json content
        # Get useful data from json content
        products_json = ast.literal_eval(products_json)
        products = []
        for product in products_json["Products"]:
            products.append(
                {"name": product["Name"], "description": product["Subtitle"], "storeName": "tezolmarket.com",
                 "okPrice": product["UnitPrice"], "thumbImage": product["ThumbImageUrl"], "caption": product["Slug"],
                 "discount": product["StoreDiscount"]})
        print({"Products": products})
        return {"Products": products}

    async def web_scrape_task(self, url: str, params: dict) -> None:
        # Download json data of url page
        content = await self.download(page_url=url, params=params)
        products_parsed_data = await self.parse_products(content)
        # Send parsed products data to server
        await self.send_data(products_parsed_data)

    def task(self, url: str, params: dict) -> None:
        asyncio.run(self.web_scrape_task(url, params))

    async def run(self) -> None:
        home_page_content = await self.download(self.url, method="GET")
        self.home_page_content = home_page_content
        await self.parse_urls()
        # Run the crawling
        # Add task of CategoryId
        tasks = []
        with concurrent.futures.ThreadPoolExecutor(cpu_count()) as executor:
            for category in self.urls:
                tasks.append(executor.submit(
                    self.task, f"{self.url}/Home/GetProductQueryResult/",
                    {"CategoryId": int(category.split("/")[2])}
                    # Function to perform
                ))

        concurrent.futures.wait(tasks)

