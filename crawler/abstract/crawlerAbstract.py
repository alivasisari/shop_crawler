from abc import abstractmethod, ABC


class CrawlerAbstract(ABC):
    @abstractmethod
    async def download(self, *args, **kwargs):
        pass

    @abstractmethod
    async def send_data(self, *args, **kwargs):
        pass

    @abstractmethod
    async def parse_urls(self):
        pass

    @abstractmethod
    async def parse_products(self, *args, **kwargs):
        pass

    @abstractmethod
    async def web_scrape_task(self, *args, **kwargs):
        pass

    @abstractmethod
    async def task(self, *args, **kwargs):
        pass

    @abstractmethod
    async def run(self):
        pass
