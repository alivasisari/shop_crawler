import asyncio
import aiohttp
from bs4 import BeautifulSoup
import json
import concurrent.futures
from multiprocessing import cpu_count
from threading import current_thread
from ..abstract.crawlerAbstract import CrawlerAbstract


class Crawler(CrawlerAbstract):
    def __init__(self):
        self.url = "https://okala.com/"
        self.urls = set()
        self.backend_url = "http://127.0.0.1:8000/products/add/"
        self.home_page_content = None
        self.max_page_number = 1

    async def download(self, page_url) -> str:
        # Send post request to url
        # Get html content
        print(f"Beginning downloading {current_thread()}")
        async with aiohttp.ClientSession() as session:
            async with session.get(url=f"{page_url}") as response:
                if response.status == 200:
                    content = await response.text()
                else:
                    raise ValueError('Error')
        print(f"Finished downloading {page_url} {current_thread()}")
        return content

    async def send_data(self, params: dict) -> None:
        # Get parsed data
        # Send data to database
        print(f"Begin sending {self.backend_url}")
        async with aiohttp.ClientSession() as session:
            async with session.request(method='POST', url=self.backend_url, json=params) as response:
                if response.status == 200:
                    print("Successful")
                else:
                    raise ValueError('Error to download content')
        print(f"Finished sending {self.backend_url}")

    async def parse_urls(self) -> None:
        # Extract all url of a website
        if self.home_page_content is not None:
            parser = BeautifulSoup(self.home_page_content)
            html_parse_script = parser.find("script", attrs={"id": "__NEXT_DATA__"}).text
            products_parsed_full_data = json.loads(html_parse_script)["props"]["res"]["data"]["childs"]
            for category in products_parsed_full_data:
                self.urls.add(category["url"])
        else:
            raise ValueError('Content is None')

    async def parse_products(self, products_category_url: str) -> dict:
        # Parse page html content
        parser = BeautifulSoup(products_category_url)
        content_script = parser.find("script", attrs={"id": "__NEXT_DATA__"}).text
        products_data = json.loads(content_script)["props"]["pageProps"]["data"]["getListOfProduct"]["data"][
            "entities"]
        # Get useful data from html content
        products = []
        for product in products_data:
            print(product)
            products.append(
                {"name": product["name"], "description": product["description"], "storeName": "okala.com",
                 "okPrice": product["okPrice"], "thumbImage": product["thumbImage"], "caption": product["caption"],
                 "discount": product["discountPercent"]})
        return {"Products": products}

    async def web_scrape_task(self, products_category_url: str) -> None:
        # Download html data of url page
        html = await self.download(products_category_url)
        # Parse html content
        products_parsed_data = await self.parse_products(html)
        # Send parsed products data to server
        await self.send_data(products_parsed_data)

    def task(self, products_category_url: str) -> None:
        # Call web_scraper to get useful data
        asyncio.run(self.web_scrape_task(products_category_url))

    async def run(self) -> None:
        self.home_page_content = await self.download(self.url)
        await self.parse_urls()
        # Run the crawling
        # Add task of CategoryId
        tasks = []
        with concurrent.futures.ThreadPoolExecutor(cpu_count()) as executor:
            for category in self.urls:
                for page_number in range(1, self.max_page_number + 1):
                    tasks.append(executor.submit(
                        self.task, f"{self.url}{category}?page={page_number}/"  # Function to perform
                    ))
        concurrent.futures.wait(tasks)

