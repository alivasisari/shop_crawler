import asyncio

from crawler.tezolmarket.crawler import Crawler as TezolCrawler
from crawler.okala.crawler import Crawler as OkalaCrawler


if __name__ == '__main__':
    crawl = TezolCrawler()
    asyncio.run(crawl.run())
    crawl = OkalaCrawler()
    asyncio.run(crawl.run())

